# ren

> 忍，能也。从心、刃声。

[![GoDoc](https://godoc.org/bitbucket.org/ai69/ren?status.svg)](https://godoc.org/bitbucket.org/ai69/ren)

wrapper for [**ulule/limiter**](https://github.com/ulule/limiter) to provide simple rate limiter for client-side app. inspired by [**kimsudo/wulimt**](https://github.com/kimsudo/wulimt).
