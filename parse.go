package ren

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	limiter "github.com/ulule/limiter/v3"
)

// ParseRate parses formatted rate string (limit-period) like 1-10s (1 for every 10s), 60-m (60 per minute).
func ParseRate(formatted string) (*limiter.Rate, error) {
	values := strings.Split(formatted, "-")
	if len(values) != 2 {
		return nil, fmt.Errorf("incorrect format '%s'", formatted)
	}

	periodMap := map[string]time.Duration{
		"S": time.Second,    // Second
		"M": time.Minute,    // Minute
		"H": time.Hour,      // Hour
		"D": time.Hour * 24, // Day
	}

	// split string like "2-10s" into limit="2" and period="10s"
	limit, period := strings.TrimSpace(values[0]), strings.TrimSpace(values[1])

	// convert limit string like "2" into 2
	numLimit, err := strconv.ParseInt(limit, 10, 64)
	if err != nil {
		return nil, fmt.Errorf("incorrect limit '%s': %v", limit, err)
	} else if numLimit < 1 {
		return nil, fmt.Errorf("invalid limit '%s': %d", limit, numLimit)
	}

	// convert period string like "10s" into 10*time.Second
	var numPeriod time.Duration
	if lp := len(period); lp == 0 {
		return nil, fmt.Errorf("incorrect period '%s': blank", period)
	} else {
		// treat "s" as "1s"
		base := int64(1)
		if lp > 1 {
			pn, err := strconv.ParseInt(period[0:lp-1], 10, 64)
			if err != nil {
				return nil, fmt.Errorf("incorrect period '%s': %v", limit, err)
			}
			base = pn
		}

		periodUnit := strings.ToUpper(string(period[lp-1]))
		periodDuration, ok := periodMap[periodUnit]
		if !ok {
			return nil, fmt.Errorf("invalid period unit '%s'", period)
		}
		numPeriod = time.Duration(base) * periodDuration

		// reformat
		formatted = fmt.Sprintf("%d-%d%s", numLimit, base, periodUnit)
	}

	return &limiter.Rate{
		Formatted: formatted,
		Limit:     numLimit,
		Period:    numPeriod,
	}, nil
}
