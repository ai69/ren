// Package ren is a wrapper for ulule/limiter to provide simple rate limiter for client-side app.
package ren

import (
	"fmt"
	"sync"
	"time"

	limiter "github.com/ulule/limiter/v3"
	"github.com/ulule/limiter/v3/drivers/store/memory"
)

const (
	defaultBufferRate = 0.4
)

var limiters = sync.Map{}

// Get returns a rate limiter instance by name, nil will be returned if not exists.
func Get(name string) *RateLimiter {
	if rl, ok := limiters.Load(name); ok {
		return rl.(*RateLimiter)
	}
	return nil
}

// GetOrNew returns a rate limiter instance by name, or create a new one if not exists.
func GetOrNew(name string, periodSec, times int64) *RateLimiter {
	if rl, ok := limiters.Load(name); ok {
		return rl.(*RateLimiter)
	} else {
		rl := &RateLimiter{
			n: name,
			l: limiter.New(memory.NewStore(), limiter.Rate{
				Period: time.Duration(periodSec) * time.Second,
				Limit:  times,
			}),
			b: defaultBufferRate,
		}
		limiters.Store(name, rl)
		return rl
	}
}

// GetOrNewFromFormatted returns a rate limiter instance by name, or create a new one with formatted rate expression if not exists.
// It panics if fails to parse formatted rate string.
func GetOrNewFromFormatted(name, expr string) *RateLimiter {
	if rl, ok := limiters.Load(name); ok {
		return rl.(*RateLimiter)
	} else {
		rate, err := ParseRate(expr)
		if err != nil {
			panic(err)
		} else if rate == nil {
			panic(fmt.Errorf("got nil limiter.Rate"))
		}
		rl := &RateLimiter{
			n: name,
			l: limiter.New(memory.NewStore(), *rate),
			b: defaultBufferRate,
		}
		limiters.Store(name, rl)
		return rl
	}
}
