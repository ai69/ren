package ren

import (
	"context"
	"fmt"
	"time"

	"github.com/1set/gut/yrand"
	limiter "github.com/ulule/limiter/v3"
)

// RateLimiter represents an instance of rate limiter.
type RateLimiter struct {
	n string
	l *limiter.Limiter
	b float64
}

func (rl RateLimiter) String() string {
	return fmt.Sprintf("{limiter:%s, rate:%v/%v}", rl.n, rl.l.Rate.Limit, rl.l.Rate.Period)
}

// GetBufferRate returns buffer rate of the rate limiter.
func (rl *RateLimiter) GetBufferRate() float64 {
	return rl.b
}

// SetBufferRate sets buffer rate of the rate limiter, value <= 0 will disable time buffer in WaitMore() as well.
func (rl *RateLimiter) SetBufferRate(br float64) {
	rl.b = br
}

// Wait blocks current goroutine to ensure the rate limit requirement is fulfilled.
func (rl *RateLimiter) Wait() {
	rl.waitOnLimiter(-1)
}

// WaitMore blocks current goroutine to ensure the rate limiting requirement is fulfilled, and wait a little longer.
func (rl *RateLimiter) WaitMore() {
	rl.waitOnLimiter(rl.b)
}

// Estimate returns average waiting time of the rate limiter.
func (rl *RateLimiter) Estimate() time.Duration {
	return time.Duration(((1 + 0.5*rl.b) * float64(rl.l.Rate.Period)) / float64(rl.l.Rate.Limit))
}

func (rl *RateLimiter) waitOnLimiter(bufferRate float64) {
	key := "default"
	for {
		ctx, _ := rl.l.Get(context.Background(), key)
		if ctx.Reached {
			interval := time.Unix(ctx.Reset, 0).Sub(time.Now())
			if interval > 0 {
				if bufferRate > 0 {
					rand, _ := yrand.Float64()
					interval = time.Duration((1 + bufferRate*rand) * float64(interval))
				}
				time.Sleep(interval)
			} else {
				time.Sleep(50 * time.Millisecond)
			}
		} else {
			break
		}
	}
}
