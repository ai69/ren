module bitbucket.org/ai69/ren

go 1.16

require (
	github.com/1set/gut v0.0.0-20201117175203-a82363231997
	github.com/ulule/limiter/v3 v3.8.0
)
