package ren

import (
	"fmt"
	"testing"
	"time"
)

func TestRateLimiter_Get(t *testing.T) {
	key, expr := "name_key", "1-5m"
	rl1 := GetOrNewFromFormatted(key, expr)
	rl2 := GetOrNewFromFormatted(key, expr)
	rl3 := GetOrNewFromFormatted(key+"_test", expr)
	if rl1 != rl2 {
		t.Errorf("rl1 and rl2 should be the same instance")
	}
	if rl2 == rl3 {
		t.Errorf("rl1 and rl2 should be the different instance")
	}
	t.Logf("RL1:%p, RL2:%p, RL3:%p", rl1, rl2, rl3)
}

func TestRateLimiter_String(t *testing.T) {
	tests := []struct {
		name string
		expr string
	}{
		{"t1", "1-5m"},
		{"t2", "5-m"},
		{"t3", "6-h"},
		{"t4", "7-D"},
		{"t5", "8-s"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rl := GetOrNewFromFormatted(tt.name, tt.expr)
			t.Logf("String() raw = %v | %v, got = %v", tt.name, tt.expr, rl)
		})
	}
}

func TestRateLimiter_Estimate(t *testing.T) {
	tests := []struct {
		expr    string
		wantEst time.Duration
	}{
		{"1-5m", 375 * time.Second},
		{"5-m", 15 * time.Second},
		{"6-h", 750 * time.Second},
		{"10-s", 125 * time.Millisecond},
	}
	for idx, tt := range tests {
		t.Run(fmt.Sprintf("Case#%d", idx+1), func(t *testing.T) {
			rl := GetOrNewFromFormatted(tt.expr, tt.expr)
			rl.SetBufferRate(0.5)
			if est := rl.Estimate(); est != tt.wantEst {
				t.Errorf("Estimate() got = %v, wantErr %v", est, tt.wantEst)
				return
			}
		})
	}
}

func TestRateLimiter_Wait(t *testing.T) {
	var (
		equalDuration = func(act, exp time.Duration) bool {
			delta := time.Duration(float64(exp) * 0.001)
			min, max := act-delta, act+delta
			return min <= act && act <= max
		}
		s0 = 0 * time.Second
		s1 = 1 * time.Second
		s5 = 5 * time.Second
	)
	tests := []struct {
		expr         string
		wantInterval []time.Duration
	}{
		{"1-1s", []time.Duration{s0, s1, s1, s1, s1, s1}},
		{"1-5s", []time.Duration{s0, s5, s5, s5, s5, s5}},
		{"2-5s", []time.Duration{s0, s0, s5, s0, s5, s0}},
	}
	for idx, tt := range tests {
		t.Run(fmt.Sprintf("Case#%d", idx+1), func(t *testing.T) {
			rl := GetOrNewFromFormatted(tt.expr, tt.expr)
			var (
				last = time.Now()
			)
			for i := 0; i <= 5; i++ {
				rl.Wait()
				elapsed := time.Since(last)
				last = time.Now()
				if expected := tt.wantInterval[i]; !equalDuration(elapsed, expected) {
					t.Errorf("Expr[%s]#%d, actual = %v, expected = %v", tt.expr, i, elapsed, expected)
				}
			}
		})
	}
}
